﻿fffffffpackage com.chinasoft.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.RequestMapping;

import com.chinasoft.entity.Admin;
import com.chinasoft.service.AdminService;
@Controller
public class AdminContrller {
	@Autowired
	private AdminService adminService;

	@RequestMapping("/login")
	public String login() {

		return "login";
	}

	//RussellBranch Second modify

	//CaraBranch modify


//miya first combin master

	@RequestMapping("/checkLogin")
	public String checkLogin(Admin admin, Model model) {
		
		Admin admin2 = adminService.findAdmin(admin);
		
		if(admin2!=null) {
			model.addAttribute("admin2", admin);
			return "/jsp/index.jsp";
		}else{
			return "/jsp/fail.jsp";
		}
	}
	// 注销方法
		@RequestMapping("/outLogin")
		public String outLogin(HttpSession session) {
			// 通过session.invalidata()方法来注销当前的session
			session.invalidate();
			return "/jsp/login.jsp";
		}

}
